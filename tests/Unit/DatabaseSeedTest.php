<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\File;
use App\Models\Post;
use App\Models\Brand;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Category;
use App\Models\Promotion;
use App\Models\OrderStatus;
use Database\Seeders\FileSeeder;
use Database\Seeders\PostSeeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\AdminSeeder;
use Database\Seeders\BrandSeeder;
use Database\Seeders\OrderSeeder;
use Database\Seeders\PaymentSeeder;
use Database\Seeders\ProductSeeder;
use Database\Seeders\CategorySeeder;
use Database\Seeders\PromotionSeeder;
use Illuminate\Support\Facades\Schema;
use Database\Seeders\OrderStatusSeeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;

class DatabaseSeedTest extends TestCase
{

    use InteractsWithDatabase;

    /**
     * testing admin seeder
     *
     * @return void
     */
    public function test_admin_seed()
    {
        $this->seed(AdminSeeder::class);
        $this->assertDatabaseCount(User::class, 1);
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * testing brand seeder
     *
     * @return void
     */
    public function test_brand_seed()
    {
        $this->seed(BrandSeeder::class);
        $this->assertDatabaseCount(Brand::class, 15);
        Schema::disableForeignKeyConstraints();
        Brand::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * testing category seeder
     *
     * @return void
     */
    public function test_category_seed()
    {
        $this->seed(CategorySeeder::class);
        $this->assertDatabaseCount(Category::class, 10);
        Schema::disableForeignKeyConstraints();
        Category::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * testing file seeder
     *
     * @return void
     */
    public function test_file_seed()
    {
        $this->seed(FileSeeder::class);
        $this->assertDatabaseCount(File::class, 20);
        $files = File::all();
        foreach ($files as $file) {
            $this->assertFileExists($file->path);
            $fileName = basename($file->path);
            Storage::delete("pet-store/$fileName");
        }
        Schema::disableForeignKeyConstraints();
        File::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * testing order seeder
     *
     * @return void
     */
    public function test_order_seed()
    {
        $this->seed([
            UserSeeder::class,
            BrandSeeder::class,
            CategorySeeder::class,
            FileSeeder::class,
            OrderStatusSeeder::class,
            PaymentSeeder::class,
            ProductSeeder::class,
            OrderSeeder::class,
        ]);
        $this->assertDatabaseCount(Order::class, 50);
        foreach (File::all() as $file) {
            $fileName = basename($file->path);
            Storage::delete("pet-store/$fileName");
        }
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Brand::truncate();
        Category::truncate();
        Order::truncate();
        Product::truncate();
        OrderStatus::truncate();
        Payment::truncate();
        File::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * testing order status seeder
     *
     * @return void
     */
    public function test_order_status_seed()
    {
        $this->seed(OrderStatusSeeder::class);
        $this->assertDatabaseCount(OrderStatus::class, 5);
        Schema::disableForeignKeyConstraints();
        OrderStatus::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * testing payment seeder
     *
     * @return void
     */
    public function test_payment_seed()
    {
        $this->seed(PaymentSeeder::class);
        $this->assertDatabaseCount(Payment::class, 20);
        Schema::disableForeignKeyConstraints();
        Payment::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * testing post seeder
     *
     * @return void
     */
    public function test_post_seed()
    {
        $this->seed([
            UserSeeder::class,
            FileSeeder::class,
            PostSeeder::class
        ]);
        $this->assertDatabaseCount(Post::class, 10);
        foreach (File::all() as $file) {
            $fileName = basename($file->path);
            Storage::delete("pet-store/$fileName");
        }
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        File::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * testing product seeder
     *
     * @return void
     */
    public function test_product_seed()
    {
        $this->seed([
            BrandSeeder::class,
            CategorySeeder::class,
            FileSeeder::class,
            PaymentSeeder::class,
            ProductSeeder::class,
        ]);
        $this->assertDatabaseCount(Product::class, 50);
        foreach (File::all() as $file) {
            $fileName = basename($file->path);
            Storage::delete("pet-store/$fileName");
        }
        Schema::disableForeignKeyConstraints();
        Product::truncate();
        Brand::truncate();
        Category::truncate();
        Payment::truncate();
        File::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * testing promotion seeder
     *
     * @return void
     */
    public function test_promotion_seed()
    {
        $this->seed([
            FileSeeder::class,
            PromotionSeeder::class
        ]);
        $this->assertDatabaseCount(Promotion::class, 10);
        foreach (File::all() as $file) {
            $fileName = basename($file->path);
            Storage::delete("pet-store/$fileName");
        }
        Schema::disableForeignKeyConstraints();
        Promotion::truncate();
        File::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * testing user seeder
     *
     * @return void
     */
    public function test_user_seed()
    {
        $this->seed(UserSeeder::class);
        $this->assertDatabaseCount(User::class, 10);
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
