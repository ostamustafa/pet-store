<?php

namespace Tests\Feature\Http\Controllers\Api\V1;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;

class AdminControllerTest extends TestCase
{
    /**
     * feature test for a successful admin login
     *
     * @return void
     */
    public function test_successful_admin_login()
    {
        $response = $this->postJson(route('api.v1.admin.login'), [
            'email' => 'admin@buckhill.co.uk',
            'password' => 'admin'
        ]);
        $response->assertOk();
        $response->assertJson(fn(AssertableJson $json) => $json->has('data.token')
            ->where('success', 1)
            ->where('error', null)
            ->where('errors', [])
            ->where('extra', []));
    }

    public function test_invalid_admin_login()
    {
        $response = $this->postJson(route('api.v1.admin.login'), [
            'email' => 'admin@buckhill.co.uk',
            'password' => 'admins'
        ]);
        $response->assertUnprocessable();
        $response->assertJson(fn(AssertableJson $json) => $json->where('success', 0)
            ->where('data', [])
            ->where('error', 'Failed to authenticate user')
            ->where('errors', [])
            ->where('trace', []));
    }

    public function test_successful_admin_logout()
    {
        $loginResponse = $this->postJson(route('api.v1.admin.login'), [
            'email' => 'admin@buckhill.co.uk',
            'password' => 'admin'
        ]);
        $token = $loginResponse->json('data.token');
        $logoutResponse = $this->getJson(route('api.v1.admin.logout'), [
            'Authorization' => "Bearer $token"
        ]);
        $logoutResponse->assertOk();
        $logoutResponse->assertJson(fn(AssertableJson $json) => $json->where('success', 1)
            ->where('data', [])
            ->where('error', null)
            ->where('errors', [])
            ->where('extra', []));
    }

    public function test_invalid_token_admin_logout()
    {
        $token = "this_is_an_invalid_token";
        $logoutResponse = $this->getJson(route('api.v1.admin.logout'), [
            'Authorization' => "Bearer $token"
        ]);
        $logoutResponse->assertUnauthorized();
        $logoutResponse->assertJson(fn(AssertableJson $json) => $json->where('success', 0)
            ->where('data', [])
            ->where('error', "Failed to authenticate user")
            ->where('errors', [])
            ->where('trace', []));
    }

    public function test_successful_admin_creation()
    {
        $token = $this->postJson(route('api.v1.admin.login'), [
            'email' => 'admin@buckhill.co.uk',
            'password' => 'admin'
        ])->json('data.token');
        $userCount = User::count();
        $response = $this->withToken($token)
            ->postJson(route('api.v1.admin.create'), [
                'first_name' => 'admin',
                'last_name' => 'admin',
                'email' => 'admin2@buckhill.co.uk',
                'password' => 'admin',
                'password_confirmation' => 'admin',
                'address' => 'some address',
                'phone_number' => '12345678',
                'marketing' => false
            ]);
        $response->assertOk();
        $response->assertJson(fn(AssertableJson $json) => $json->where('success', 1)
            ->has('data.admin', fn(AssertableJson $json) => $json->where('first_name', 'admin')
                ->where('last_name', 'admin')
                ->where('email', 'admin2@buckhill.co.uk')
                ->where('address', 'some address')
                ->where('phone_number', '12345678')
                ->where('is_marketing', false)
                ->etc())
            ->where('error', null)
            ->where('errors', [])
            ->where('extra', [])
            ->etc());
        $this->assertDatabaseCount(User::class, $userCount + 1);
        User::whereEmail('admin2@buckhill.co.uk')->delete();
    }

    public function test_invalid_admin_creation()
    {
        $token = $this->postJson(route('api.v1.admin.login'), [
            'email' => 'admin@buckhill.co.uk',
            'password' => 'admin'
        ])->json('data.token');
        $response = $this->withToken($token)
            ->postJson(route('api.v1.admin.create'), [
                'first_name' => 'ad',
                'last_name' => 'ad',
                'email' => 'admin@buckhill.co.uk',
                'password' => 'admin',
                'password_confirmation' => 'admins',
                'phone_number' => '12345678',
                'marketing' => false
            ]);
        $response->assertUnprocessable();
        $response->assertJson(fn(AssertableJson $json) => $json->where('success', 0)
            ->where('data', [])
            ->where('error', 'Failed Validation')
            ->has('errors', fn(AssertableJson $json) => $json->where('email.0', 'The email has already been taken.')
                ->where('first_name.0', 'The first name must be at least 3 characters.')
                ->where('last_name.0', 'The last name must be at least 3 characters.')
                ->where('password.0', 'The password confirmation does not match.')
                ->where('address.0', 'The address field is required.')
                ->etc())
            ->where('trace', []));
    }

    public function test_admin_creation_without_token()
    {
        $response = $this->postJson(route('api.v1.admin.create'), [
            'first_name' => 'admin',
            'last_name' => 'admin',
            'email' => 'admin2@buckhill.co.uk',
            'password' => 'admin',
            'password_confirmation' => 'admin',
            'address' => 'some address',
            'phone_number' => '12345678',
            'marketing' => false
        ]);
        $response->assertUnauthorized();
        $response->assertJson(fn(AssertableJson $json) => $json->where('success', 0)
            ->where('data', [])
            ->where('error', 'Failed to authenticate user')
            ->where('errors', [])
            ->where('trace', []));
    }

    public function test_successful_user_edit()
    {
        $token = $this->postJson(route('api.v1.admin.login'), [
            'email' => 'admin@buckhill.co.uk',
            'password' => 'admin'
        ])->json('data.token');
        $randomUserUuid = User::inRandomOrder()->first()->uuid;
        $response = $this->withToken($token)
            ->putJson(route('api.v1.admin.user_edit', $randomUserUuid), [
                'first_name' => 'edited',
                'last_name' => 'edited',
                'email' => 'edited@email.com',
                'password' => 'userpassword',
                'password_confirmation' => 'userpassword',
                'address' => 'edited',
                'phone_number' => '123456789',
                'marketing' => false
            ]);
        $response->assertOk();
        $response->assertJson(fn(AssertableJson $json) => $json->where('success', 1)
            ->has('data.user', fn(AssertableJson $json) => $json->where('first_name', 'edited')
                ->where('last_name', 'edited')
                ->where('email', 'edited@email.com')
                ->where('address', 'edited')
                ->where('phone_number', '123456789')
                ->where('is_marketing', false)
                ->etc())
            ->where('error', null)
            ->where('errors', [])
            ->where('extra', [])
            ->etc());
    }

    public function test_invalid_user_edit()
    {
        $token = $this->postJson(route('api.v1.admin.login'), [
            'email' => 'admin@buckhill.co.uk',
            'password' => 'admin'
        ])->json('data.token');
        $randomUserUuid = User::inRandomOrder()->first()->uuid;
        $response = $this->withToken($token)
            ->putJson(route('api.v1.admin.user_edit', $randomUserUuid), [
                'first_name' => 'ad',
                'last_name' => 'ad',
                'email' => 'edited@email.com',
                'password' => 'userpassword',
                'password_confirmation' => 'userpasswords',
                'phone_number' => '123456789',
                'marketing' => false
            ]);
        $response->assertUnprocessable();
        $response->assertJson(fn(AssertableJson $json) => $json->where('success', 0)
            ->where('data', [])
            ->where('error', 'Failed Validation')
            ->has(
                'errors',
                fn(AssertableJson $json) => $json->where(
                    'first_name.0',
                    'The first name must be at least 3 characters.'
                )
                    ->where('last_name.0', 'The last name must be at least 3 characters.')
                    ->where('password.0', 'The password confirmation does not match.')
                    ->where('address.0', 'The address field is required.')
                    ->etc()
            )
            ->where('trace', []));
    }

    public function test_user_edit_without_token()
    {
        $response = $this->putJson(route('api.v1.admin.user_edit'), [
            'first_name' => 'edited',
            'last_name' => 'edited',
            'email' => 'edited@email.com',
            'password' => 'userpassword',
            'password_confirmation' => 'userpassword',
            'address' => 'edited',
            'phone_number' => '123456789',
            'marketing' => false
        ]);
        $response->assertUnauthorized();
        $response->assertJson(fn(AssertableJson $json) => $json->where('success', 0)
            ->where('data', [])
            ->where('error', 'Failed to authenticate user')
            ->where('errors', [])
            ->where('trace', []));
    }
}
