<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'address',
        'products',
        'amount',
        'delivery_fee',
        'order_status_id',
        'payment_id',
        'shipped_at',
        'user_id'
    ];

    protected $casts = [
        'address' => 'json',
        'products' => 'json',
        'shipped_at' => 'datetime',
        'delivery_fee' => 'float',
        'amount' => 'float',
    ];

    public function payment(): BelongsTo
    {
        return $this->belongsTo(Payment::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function orderStatus(): BelongsTo
    {
        return $this->belongsTo(OrderStatus::class);
    }
}
