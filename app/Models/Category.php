<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'slug',
        'title',
    ];

    public function products(): HasMany
    {
        return $this->hasMany(Product::class, 'category_uuid');
    }
}
