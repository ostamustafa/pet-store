<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'title',
        'metadata',
        'content',
        'slug'
    ];

    protected $casts = [
        'metadata' => 'json'
    ];
}
