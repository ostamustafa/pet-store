<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'details',
        'type'
    ];

    protected $casts = [
        'details' => 'json'
    ];

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }
}
