<?php

namespace App\Providers;

use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Illuminate\Support\ServiceProvider;

class JWTServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $key = explode(':', config('app.key'))[1];
        $this->app->bind(Configuration::class, function () use ($key) {
            return Configuration::forAsymmetricSigner(
                new Sha256(),
                InMemory::plainText('pet-store'),
                InMemory::base64Encoded($key)
            );
        });
    }
}
