<?php

namespace App\Http\Controllers\Api\V1;

use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    /**
     * @OA\Post(
     *    path="/api/v1/user/login",
     *    summary="Login a User account",
     *    operationId="user-login",
     *    tags={"User"},
     *    @OA\RequestBody(
     *        required=true,
     *        @OA\MediaType(
     *            mediaType="application/x-www-form-urlencoded",
     *            @OA\Schema(
     *                type="object",
     *                required={
     *                    "email",
     *                    "password"
     *                },
     *                @OA\Property(
     *                    property="email",
     *                    type="string",
     *                    description="User email address"
     *                ),
     *                @OA\Property(
     *                    property="password",
     *                    type="string",
     *                    description="User password"
     *                )
     *            )
     *        )
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        try {
            $validator = validator($request->only('email', 'password'), [
                'email' => 'required|email|exists:users',
                'password' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->failedResponse('Failed to authenticate user', $validator->failed(), [], 422);
            }
            $validated = $validator->validated();
            $user = User::isNotAdmin()->whereEmail($validated['email'])->first();

            if (!Hash::check($validated['password'], $user->password)) {
                return $this->failedResponse('Failed to authenticate user', [], [], 422);
            }

            $token = $this->getToken($user);

            return $this->successfulResponse(['token' => $token->toString()]);
        } catch (Exception $e) {
            return $this->failedResponse($e->getMessage(), [], $e->getTrace(), 500);
        }
    }


    /**
     * @OA\Get(
     *    path="/api/v1/user/logout",
     *    summary="Logout a User account",
     *    operationId="user-logout",
     *    tags={"User"},
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        $user = auth()->user();
        $user->tokens()->delete();
        return $this->successfulResponse([]);
    }

    /**
     * @OA\Delete(
     *    path="/api/v1/user",
     *    summary="Delete a User account",
     *    operationId="user-delete",
     *    tags={"User"},
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $user = auth()->user();
        $user->orders()->delete();
        $user->delete();
        return $this->successfulResponse([]);
    }

    /**
     * @OA\Get(
     *    path="/api/v1/user",
     *    summary="View a User account",
     *    operationId="user-read",
     *    tags={"User"},
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = auth()->user();
        return $this->successfulResponse($user);
    }

    /**
     * @OA\Put(
     *    path="/api/v1/user/edit",
     *    summary="Update a user account",
     *    operationId="user-update",
     *    tags={"User"},
     *    @OA\RequestBody(
     *        required=true,
     *        @OA\MediaType(
     *            mediaType="application/x-www-form-urlencoded",
     *            @OA\Schema(
     *                type="object",
     *                required={
     *                    "first_name",
     *                    "last_name",
     *                    "email",
     *                    "password",
     *                    "password_confirmation",
     *                    "avatar",
     *                    "address",
     *                    "phone_number",
     *                    "marketing"
     *                },
     *                @OA\Property(
     *                    property="first_name",
     *                    type="string",
     *                    description="User first name"
     *                ),
     *                @OA\Property(
     *                    property="last_name",
     *                    type="string",
     *                    description="User last name"
     *                ),
     *                @OA\Property(
     *                    property="email",
     *                    type="string",
     *                    description="User email"
     *                ),
     *                @OA\Property(
     *                    property="password",
     *                    type="string",
     *                    description="User password"
     *                ),
     *                @OA\Property(
     *                    property="password_confirmation",
     *                    type="string",
     *                    description="User password"
     *                ),
     *                @OA\Property(
     *                    property="avatar",
     *                    type="string",
     *                    description="Avatar image UUID"
     *                ),
     *                @OA\Property(
     *                    property="address",
     *                    type="string",
     *                    description="User main address"
     *                ),
     *                @OA\Property(
     *                    property="phone_number",
     *                    type="string",
     *                    description="User main phone number"
     *                ),
     *                @OA\Property(
     *                    property="marketing",
     *                    type="boolean",
     *                    description="User marketing preference"
     *                ),
     *            )
     *        )
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = auth()->user();
        $validator = $this->userValidation($request, false);
        if ($validator->fails()) {
            return $this->failedResponse('Failed Validation', $validator->errors(), [], 422);
        }
        try {
            $data = $validator->validated();
            $data['is_admin'] = false;
            $data['password'] = Hash::make($data['password']);
            $data['is_marketing'] = $data['marketing'];
            unset($data['marketing']);
            $user->update($data);
            return $this->successfulResponse(['user' => $user]);
        } catch (Exception $e) {
            return $this->failedResponse('Failed to update user', [$e->getMessage()], [], 422);
        }
    }

    /**
     * @OA\Get(
     *    path="/api/v1/user/orders",
     *    summary="List all user orders",
     *    operationId="user-orders-listing",
     *    tags={"User"},
     *    @OA\Parameter(
     *        name="page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(
     *        name="limit",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(
     *        name="sortBy",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Parameter(
     *        name="desc",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="boolean")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function orders(Request $request)
    {
        $user = auth()->user();
        $orders = $user->orders()->getQuery();
        return $this->paginate($request, $orders, route('api.v1.user.orders'));
    }

    /**
     * @OA\Post(
     *    path="/api/v1/user/create",
     *    summary="Create a User account",
     *    operationId="user-create",
     *    tags={"User"},
     *    @OA\RequestBody(
     *        required=true,
     *        @OA\MediaType(
     *            mediaType="application/x-www-form-urlencoded",
     *            @OA\Schema(
     *                type="object",
     *                required={
     *                    "first_name",
     *                    "last_name",
     *                    "email",
     *                    "password",
     *                    "password_confirmation",
     *                    "avatar",
     *                    "address",
     *                    "phone_number",
     *                    "marketing"
     *                },
     *                @OA\Property(
     *                    property="first_name",
     *                    type="string",
     *                    description="User first name"
     *                ),
     *                @OA\Property(
     *                    property="last_name",
     *                    type="string",
     *                    description="User last name"
     *                ),
     *                @OA\Property(
     *                    property="email",
     *                    type="string",
     *                    description="User email"
     *                ),
     *                @OA\Property(
     *                    property="password",
     *                    type="string",
     *                    description="User password"
     *                ),
     *                @OA\Property(
     *                    property="password_confirmation",
     *                    type="string",
     *                    description="User password"
     *                ),
     *                @OA\Property(
     *                    property="avatar",
     *                    type="string",
     *                    description="Avatar image UUID"
     *                ),
     *                @OA\Property(
     *                    property="address",
     *                    type="string",
     *                    description="User main address"
     *                ),
     *                @OA\Property(
     *                    property="phone_number",
     *                    type="string",
     *                    description="User main phone number"
     *                ),
     *                @OA\Property(
     *                    property="is_marketing",
     *                    type="boolean",
     *                    description="User marketing preference"
     *                ),
     *            )
     *        )
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('title');
        $validator = validator($data, [
            'first_name' => 'required|string|min:3',
            'last_name' => 'required|string|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'avatar' => 'uuid',
            'address' => 'required|string',
            'phone_number' => 'required',
            'is_marketing' => 'bool'
        ]);
        if ($validator->fails()) {
            return $this->failedResponse('Failed Validation', $validator->errors(), [], 422);
        }
        try {
            $data = $validator->validated();
            $user = User::create($data);
            return $this->successfulResponse($user);
        } catch (Exception $e) {
            return $this->failedResponse('Failed to create user', [$e->getMessage()], [], 422);
        }
    }
}
