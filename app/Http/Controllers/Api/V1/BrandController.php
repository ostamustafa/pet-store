<?php

namespace App\Http\Controllers\Api\V1;

use Exception;
use App\Models\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{

    /**
     * @OA\Get(
     *    path="/api/v1/brands",
     *    summary="List all brands",
     *    operationId="brands-listings",
     *    tags={"Brands"},
     *    @OA\Parameter(
     *        name="page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(
     *        name="limit",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(
     *        name="sortBy",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Parameter(
     *        name="desc",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="boolean")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $brands = Brand::query();
        return $this->paginate($request, $brands, route('api.v1.brand.index'));
    }

    /**
     * @OA\Post(
     *    path="/api/v1/brand/create",
     *    summary="Create a new brand",
     *    operationId="brands-create",
     *    tags={"Brands"},
     *    @OA\RequestBody(
     *        required=true,
     *        @OA\MediaType(
     *            mediaType="application/x-www-form-urlencoded",
     *            @OA\Schema(
     *                type="object",
     *                required={"title"},
     *                @OA\Property(
     *                    property="title",
     *                    type="string",
     *                    description="Brand title"
     *                )
     *            )
     *        )
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('title');
        $validator = validator($data, [
            'title' => 'required|string'
        ]);
        if ($validator->fails()) {
            return $this->failedResponse('Failed Validation', $validator->errors(), [], 422);
        }
        try {
            $data = $validator->validated();
            $brand = Brand::create($data);
            return $this->successfulResponse($brand);
        } catch (Exception $e) {
            return $this->failedResponse('Failed to create brand', [$e->getMessage()], [], 422);
        }
    }

    /**
     * @OA\Get(
     *    path="/api/v1/brand/{uuid}",
     *    summary="Fetch a brand",
     *    operationId="brands-read",
     *    tags={"Brands"},
     *    @OA\Parameter(
     *        name="uuid",
     *        required=true,
     *        in="path",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param string|null $uuid
     * @return \Illuminate\Http\Response
     */
    public function show(?string $uuid = null)
    {
        if ($uuid == null) {
            return $this->failedResponse('Failed to find brand', [], [], 422);
        }
        $brand = Brand::whereUuid($uuid)->first();
        if ($brand == null) {
            return $this->failedResponse('Failed to find brand', [], [], 422);
        }
        return $this->successfulResponse($brand);
    }

    /**
     * @OA\Put(
     *    path="/api/v1/brand/{uuid}",
     *    summary="Update an existing brand",
     *    operationId="brands-update",
     *    tags={"Brands"},
     *    @OA\Parameter(
     *        name="uuid",
     *        required=true,
     *        in="path",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\RequestBody(
     *        required=true,
     *        @OA\MediaType(
     *            mediaType="application/x-www-form-urlencoded",
     *            @OA\Schema(
     *                type="object",
     *                required={"title"},
     *                @OA\Property(
     *                    property="title",
     *                    type="string",
     *                    description="Brand title"
     *                )
     *            )
     *        )
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @param string|null $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ?string $uuid = null)
    {
        if ($uuid == null) {
            return $this->failedResponse('Failed to find brand', [], [], 422);
        }
        $brand = Brand::whereUuid($uuid)->first();
        if ($brand == null) {
            return $this->failedResponse('Failed to find brand', [], [], 422);
        }
        $data = $request->only('title');
        $validator = validator($data, [
            'title' => 'required|string'
        ]);
        if ($validator->fails()) {
            return $this->failedResponse('Failed Validation', $validator->errors(), [], 422);
        }
        try {
            $data = $validator->validated();
            $brand->update($data);
            return $this->successfulResponse($brand);
        } catch (Exception $e) {
            return $this->failedResponse('Failed to update brand', [$e->getMessage()], [], 422);
        }
    }

    /**
     * @OA\Delete(
     *    path="/api/v1/brand/{uuid}",
     *    summary="Delete an existing brand",
     *    operationId="brands-delete",
     *    tags={"Brands"},
     *    @OA\Parameter(
     *        name="uuid",
     *        required=true,
     *        in="path",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param string|null $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy(?string $uuid)
    {
        if ($uuid == null) {
            return $this->failedResponse('Failed to find brand', [], [], 422);
        }
        $brand = Brand::whereUuid($uuid)->first();
        if ($brand == null) {
            return $this->failedResponse('Failed to find brand', [], [], 422);
        }
        $brand->delete();
        return $this->successfulResponse([]);
    }
}
