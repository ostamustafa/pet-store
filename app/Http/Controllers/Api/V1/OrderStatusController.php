<?php

namespace App\Http\Controllers\Api\V1;

use Exception;
use App\Models\OrderStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderStatusController extends Controller
{
    /**
     * @OA\Get(
     *    path="/api/v1/order-statuses",
     *    summary="List all order statuses",
     *    operationId="order-statuses-listings",
     *    tags={"Order Statuses"},
     *    @OA\Parameter(
     *        name="page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(
     *        name="limit",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(
     *        name="sortBy",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Parameter(
     *        name="desc",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="boolean")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $orderStatuses = OrderStatus::query();
        return $this->paginate($request, $orderStatuses, route('api.v1.order-status.index'));
    }

    /**
     * @OA\Post(
     *    path="/api/v1/order-status/create",
     *    summary="Create a new order status",
     *    operationId="order-statuses-create",
     *    tags={"Order Statuses"},
     *    @OA\RequestBody(
     *        required=true,
     *        @OA\MediaType(
     *            mediaType="application/x-www-form-urlencoded",
     *            @OA\Schema(
     *                type="object",
     *                required={"title"},
     *                @OA\Property(
     *                    property="title",
     *                    type="string",
     *                    description="Brand title"
     *                )
     *            )
     *        )
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('title');
        $validator = validator($data, [
            'title' => 'required|string'
        ]);
        if ($validator->fails()) {
            return $this->failedResponse('Failed Validation', $validator->errors(), [], 422);
        }
        try {
            $data = $validator->validated();
            $orderStatus = OrderStatus::create($data);
            return $this->successfulResponse($orderStatus);
        } catch (Exception $e) {
            return $this->failedResponse('Failed to create order status', [$e->getMessage()], [], 422);
        }
    }

    /**
     * @OA\Get(
     *    path="/api/v1/order-status/{uuid}",
     *    summary="Fetch an order status",
     *    operationId="order-statuses-read",
     *    tags={"Order Statuses"},
     *    @OA\Parameter(
     *        name="uuid",
     *        required=true,
     *        in="path",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param string|null $uuid
     * @return \Illuminate\Http\Response
     */
    public function show(?string $uuid = null)
    {
        if ($uuid == null) {
            return $this->failedResponse('Failed to find order status', [], [], 404);
        }
        $orderStatus = OrderStatus::whereUuid($uuid)->first();
        if ($orderStatus == null) {
            return $this->failedResponse('Failed to find order status', [], [], 404);
        }
        return $this->successfulResponse($orderStatus);
    }

    /**
     * @OA\Put(
     *    path="/api/v1/order-status/{uuid}",
     *    summary="Update an existing order status",
     *    operationId="order-statuses-update",
     *    tags={"Order Statuses"},
     *    @OA\Parameter(
     *        name="uuid",
     *        required=true,
     *        in="path",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\RequestBody(
     *        required=true,
     *        @OA\MediaType(
     *            mediaType="application/x-www-form-urlencoded",
     *            @OA\Schema(
     *                type="object",
     *                required={"title"},
     *                @OA\Property(
     *                    property="title",
     *                    type="string",
     *                    description="Brand title"
     *                )
     *            )
     *        )
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @param string|null $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ?string $uuid = null)
    {
        if ($uuid == null) {
            return $this->failedResponse('Failed to find order status', [], [], 404);
        }
        $orderStatus = OrderStatus::whereUuid($uuid)->first();
        if ($orderStatus == null) {
            return $this->failedResponse('Failed to find order status', [], [], 404);
        }
        $data = $request->only('title');
        $validator = validator($data, [
            'title' => 'required|string'
        ]);
        if ($validator->fails()) {
            return $this->failedResponse('Failed Validation', $validator->errors(), [], 422);
        }
        try {
            $data = $validator->validated();
            $orderStatus->update($data);
            return $this->successfulResponse($orderStatus);
        } catch (Exception $e) {
            return $this->failedResponse('Failed to update order status', [$e->getMessage()], [], 422);
        }
    }

    /**
     * @OA\Delete(
     *    path="/api/v1/order-statuses/{uuid}",
     *    summary="Delete an existing order status",
     *    operationId="order-statuses-delete",
     *    tags={"Order Statuses"},
     *    @OA\Parameter(
     *        name="uuid",
     *        required=true,
     *        in="path",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param string|null $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy(?string $uuid)
    {
        if ($uuid == null) {
            return $this->failedResponse('Failed to find order status', [], [], 404);
        }
        $orderStatus = OrderStatus::whereUuid($uuid)->first();
        if ($orderStatus == null) {
            return $this->failedResponse('Failed to find order status', [], [], 404);
        }
        $orderStatus->delete();
        return $this->successfulResponse([]);
    }
}
