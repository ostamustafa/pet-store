<?php

namespace App\Http\Controllers\Api\V1;

use Exception;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    /**
     * @OA\Post(
     *    path="/api/v1/admin/create",
     *    summary="Create a new admin",
     *    operationId="admin-create",
     *    tags={"Admin"},
     *    @OA\RequestBody(
     *        required=true,
     *        @OA\MediaType(
     *            mediaType="application/x-www-form-urlencoded",
     *            @OA\Schema(
     *                type="object",
     *                required={
     *                    "first_name",
     *                    "last_name",
     *                    "email",
     *                    "password",
     *                    "password_confirmation",
     *                    "avatar",
     *                    "address",
     *                    "phone_number",
     *                    "marketing"
     *                },
     *                @OA\Property(
     *                    property="first_name",
     *                    type="string",
     *                    description="Admin first name"
     *                ),
     *                @OA\Property(
     *                    property="last_name",
     *                    type="string",
     *                    description="Admin last name"
     *                ),
     *                @OA\Property(
     *                    property="email",
     *                    type="string",
     *                    description="Admin email"
     *                ),
     *                @OA\Property(
     *                    property="password",
     *                    type="string",
     *                    description="Admin password"
     *                ),
     *                @OA\Property(
     *                    property="password_confirmation",
     *                    type="string",
     *                    description="Admin password"
     *                ),
     *                @OA\Property(
     *                    property="avatar",
     *                    type="string",
     *                    description="Avatar image UUID"
     *                ),
     *                @OA\Property(
     *                    property="address",
     *                    type="string",
     *                    description="Admin main address"
     *                ),
     *                @OA\Property(
     *                    property="phone_number",
     *                    type="string",
     *                    description="Admin main phone number"
     *                ),
     *                @OA\Property(
     *                    property="marketing",
     *                    type="boolean",
     *                    description="Admin marketing preference"
     *                ),
     *            )
     *        )
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->userValidation($request);
        if ($validator->fails()) {
            return $this->failedResponse('Failed Validation', $validator->errors(), [], 422);
        }
        try {
            $data = $validator->validated();
            $data['is_admin'] = true;
            $data['password'] = Hash::make($data['password']);
            $data['is_marketing'] = $data['marketing'];
            $data['uuid'] = Str::uuid();
            $user = User::create($data);
        } catch (Exception $e) {
            return $this->failedResponse(
                'Failed to create user',
                ['message' => $e->getMessage()],
                $e->getTrace(),
                422
            );
        }
        return $this->successfulResponse(['admin' => $user]);
    }

    private function userValidation(Request $request, bool $isEmailUnique = true)
    {
        $data = $request->only([
            'first_name',
            'last_name',
            'email',
            'password',
            'password_confirmation',
            'avatar',
            'address',
            'phone_number',
            'marketing'
        ]);
        $emailRule = 'required|' . ($isEmailUnique ? 'unique:users' : '');
        return validator($data, [
                'first_name' => 'required|string|min:3',
                'last_name' => 'required|string|min:3',
                'email' => $emailRule,
                'password' => 'required|confirmed',
                'avatar' => 'nullable|uuid',
                'address' => 'required|string',
                'phone_number' => 'required',
                'marketing' => 'nullable|boolean'
            ]
        );
    }

    /**
     * @OA\Post(
     *    path="/api/v1/admin/login",
     *    summary="Login an Admin account",
     *    operationId="admin-login",
     *    tags={"Admin"},
     *    @OA\RequestBody(
     *        required=true,
     *        @OA\MediaType(
     *            mediaType="application/x-www-form-urlencoded",
     *            @OA\Schema(
     *                type="object",
     *                required={
     *                    "email",
     *                    "password"
     *                },
     *                @OA\Property(
     *                    property="email",
     *                    type="string",
     *                    description="Admin email address"
     *                ),
     *                @OA\Property(
     *                    property="password",
     *                    type="string",
     *                    description="Admin password"
     *                )
     *            )
     *        )
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        try {
            $validator = validator($request->only('email', 'password'), [
                'email' => 'required|email|exists:users',
                'password' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->failedResponse('Failed to authenticate user', $validator->failed(), [], 422);
            }
            $validated = $validator->validated();
            $admin = User::isAdmin()->whereEmail($validated['email'])->first();

            if (!Hash::check($validated['password'], $admin->password)) {
                return $this->failedResponse('Failed to authenticate user', [], [], 422);
            }

            $token = $this->getToken($admin);

            return $this->successfulResponse(['token' => $token->toString()]);
        } catch (Exception $e) {
            return $this->failedResponse($e->getMessage(), [], $e->getTrace(), 500);
        }
    }

    /**
     * @OA\Get(
     *    path="/api/v1/admin/user-listing",
     *    summary="List all users",
     *    operationId="admin-users-listing",
     *    tags={"Admin"},
     *    @OA\Parameter(
     *        name="page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(
     *        name="limit",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(
     *        name="sortBy",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Parameter(
     *        name="desc",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="boolean")
     *    ),
     *    @OA\Parameter(
     *        name="first_name",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Parameter(
     *        name="email",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Parameter(
     *        name="phone",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Parameter(
     *        name="address",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Parameter(
     *        name="created_at",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Parameter(
     *        name="marketing",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="boolean")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function userListing(Request $request)
    {
        $firstName = $request->input('first_name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $address = $request->input('address');
        $createdAt = $request->input('created_at');
        $marketing = $request->input('marketing');
        $users = User::isNotAdmin();
        if ($firstName) {
            $lowerFirstName = Str::lower($firstName);
            $users->where('first_name', 'like', "%$lowerFirstName%");
        }
        if ($email) {
            $users->where('email', 'like', "%$email%");
        }
        if ($phone) {
            $users->where('phone_number', 'like', "%$phone%");
        }
        if ($address) {
            $users->where('address', 'like', "%$address%");
        }
        if ($createdAt) {
            $users->whereDate('created_at', $createdAt);
        }
        if ($marketing) {
            $users->where('is_marketing', filter_var($marketing, FILTER_VALIDATE_BOOLEAN));
        }
        return $this->paginate($request, $users, route('api.v1.admin.user_listing'));
    }

    /**
     * @OA\Get(
     *    path="/api/v1/admin/logout",
     *    summary="Logout an Admin account",
     *    operationId="admin-logout",
     *    tags={"Admin"},
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        $user = auth()->user();
        $user->tokens()->delete();
        return $this->successfulResponse([]);
    }

    /**
     * @OA\Delete(
     *    path="/api/v1/admin/user-delete/{uuid}",
     *    summary="Delete a User account",
     *    operationId="admin-user-delete",
     *    tags={"Admin"},
     *    @OA\Parameter(
     *        name="uuid",
     *        required=true,
     *        in="path",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param string|null $uuid
     * @return \Illuminate\Http\Response
     */
    public function userDelete(?string $uuid = null)
    {
        if (!$uuid) {
            return $this->failedResponse('User not found', [], [], 404);
        }
        $user = User::isNotAdmin()->whereUuid($uuid)->first();
        if (!$user) {
            return $this->failedResponse('User not found', [], [], 404);
        }
        $user->orders()->delete();
        $user->delete();
        return $this->successfulResponse([]);
    }

    /**
     * @OA\Put(
     *    path="/api/v1/admin/user-edit/{uuid}",
     *    summary="Edit a user account",
     *    operationId="admin-user-edit",
     *    tags={"Admin"},
     *    @OA\Parameter(
     *        name="uuid",
     *        required=true,
     *        in="path",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\RequestBody(
     *        required=true,
     *        @OA\MediaType(
     *            mediaType="application/x-www-form-urlencoded",
     *            @OA\Schema(
     *                type="object",
     *                required={
     *                    "first_name",
     *                    "last_name",
     *                    "email",
     *                    "password",
     *                    "password_confirmation",
     *                    "avatar",
     *                    "address",
     *                    "phone_number",
     *                    "marketing"
     *                },
     *                @OA\Property(
     *                    property="first_name",
     *                    type="string",
     *                    description="Admin first name"
     *                ),
     *                @OA\Property(
     *                    property="last_name",
     *                    type="string",
     *                    description="Admin last name"
     *                ),
     *                @OA\Property(
     *                    property="email",
     *                    type="string",
     *                    description="Admin email"
     *                ),
     *                @OA\Property(
     *                    property="password",
     *                    type="string",
     *                    description="Admin password"
     *                ),
     *                @OA\Property(
     *                    property="password_confirmation",
     *                    type="string",
     *                    description="Admin password"
     *                ),
     *                @OA\Property(
     *                    property="avatar",
     *                    type="string",
     *                    description="Avatar image UUID"
     *                ),
     *                @OA\Property(
     *                    property="address",
     *                    type="string",
     *                    description="Admin main address"
     *                ),
     *                @OA\Property(
     *                    property="phone_number",
     *                    type="string",
     *                    description="Admin main phone number"
     *                ),
     *                @OA\Property(
     *                    property="marketing",
     *                    type="boolean",
     *                    description="Admin marketing preference"
     *                ),
     *            )
     *        )
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @param string|null $uuid
     * @return \Illuminate\Http\Response
     */
    public function userEdit(Request $request, ?string $uuid)
    {
        if (!$uuid) {
            return $this->failedResponse('User not found', [], [], 404);
        }
        $user = User::isNotAdmin()->whereUuid($uuid)->first();
        if (!$user) {
            return $this->failedResponse('User not found', [], [], 404);
        }
        $validator = $this->userValidation($request, false);
        if ($validator->fails()) {
            return $this->failedResponse('Failed Validation', $validator->errors(), [], 422);
        }
        try {
            $data = $validator->validated();
            $data['is_admin'] = false;
            $data['password'] = Hash::make($data['password']);
            $data['is_marketing'] = $data['marketing'];
            unset($data['marketing']);
            $user->update($data);
            return $this->successfulResponse(['user' => $user]);
        } catch (Exception $e) {
            return $this->failedResponse('Failed to update user', [$e->getMessage()], [], 422);
        }
    }
}
