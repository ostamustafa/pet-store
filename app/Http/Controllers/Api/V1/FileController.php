<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\File;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{

    /**
     * @OA\Post(
     *    path="/api/v1/file/upload",
     *    summary="Upload a file",
     *    operationId="files-upload",
     *    tags={"Files"},
     *    @OA\RequestBody(
     *        required=true,
     *        @OA\MediaType(
     *            mediaType="multipart/form-data",
     *            @OA\Schema(
     *                type="object",
     *                required={"file"},
     *                @OA\Property(
     *                    property="file",
     *                    type="string",
     *                    format="binary",
     *                    description="file to upload"
     *                )
     *            )
     *        )
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $data = $request->only('file');
        $validator = validator($data, [
            'file' => 'required|file|'
        ]);

        if ($validator->fails()) {
            return $this->failedResponse('Failed Validation', $validator->errors(), [], 422);
        }
        $path = $request->file('file')->store('public/pet-store');
        $uuid = Str::uuid();
        $name = Str::random();
        $type = Storage::mimeType($path);
        $size = $this->getSize(Storage::size($path));
        $file = File::create([
            'size' => $size,
            'path' => $path,
            'uuid' => $uuid,
            'name' => $name,
            'type' => $type
        ]);
        return $this->successfulResponse($file);
    }

    private function getSize(float $size): string
    {
        if ($size < 1024) {
            $size = number_format($size, 2);
            return "$size B";
        }
        $size = $size / 1024;
        if ($size < 1024) {
            $size = number_format($size, 2);
            return "$size KB";
        }

        $size = $size / 1024;
        if ($size < 1024) {
            $size = number_format($size, 2);
            return "$size MB";
        }
        $size = $size / 1024;
        $size = number_format($size, 2);
        return "$size GB";
    }

    /**
     * @OA\Get(
     *    path="/api/v1/file/{uuid}",
     *    summary="Read a file",
     *    operationId="files-read",
     *    tags={"Files"},
     *    @OA\Parameter(
     *        name="uuid",
     *        required=true,
     *        in="path",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param string|null $uuid
     * @return \Illuminate\Http\Response
     */
    public function show(?string $uuid)
    {
        if (!$uuid) {
            return $this->failedResponse("Failed to find file", [], [], 404);
        }
        $file = File::whereUuid($uuid)->first();
        if (!$file) {
            return $this->failedResponse("Failed to find file", [], [], 404);
        }
        return Storage::response($file->path);
    }
}
