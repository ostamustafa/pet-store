<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Post;
use App\Models\Promotion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    /**
     * @OA\Get(
     *    path="/api/v1/main/promotions",
     *    summary="List all promotions",
     *    operationId="promotions-listings",
     *    tags={"Promotions"},
     *    @OA\Parameter(
     *        name="page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(
     *        name="limit",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(
     *        name="sortBy",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Parameter(
     *        name="desc",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="boolean")
     *    ),
     *    @OA\Parameter(
     *        name="valid",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="boolean")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function promotions(Request $request)
    {
        $valid = $request->input('valid');
        $promotions = Promotion::query();
        if (filter_var($valid, FILTER_VALIDATE_BOOLEAN)) {
            $promotions->where('metadata->valid_from', '<', now());
            $promotions->where('metadata->valid_to', '>', now());
        }
        return $this->paginate($request, $promotions, route('api.v1.main.promotions'));
    }

    /**
     * @OA\Get(
     *    path="/api/v1/main/blog",
     *    summary="List all posts",
     *    operationId="posts-listings",
     *    tags={"Posts"},
     *    @OA\Parameter(
     *        name="page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(
     *        name="limit",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(
     *        name="sortBy",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Parameter(
     *        name="desc",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="boolean")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function blog(Request $request)
    {
        $posts = Post::query();
        return $this->paginate($request, $posts, route('api.v1.main.blog'));
    }

    /**
     * @OA\Get(
     *    path="/api/v1/main/blog/{uuid}",
     *    summary="Fetch a post",
     *    operationId="posts-read",
     *    tags={"Posts"},
     *    @OA\Parameter(
     *        name="uuid",
     *        required=true,
     *        in="path",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param string|null $uuid
     * @return \Illuminate\Http\Response
     */
    public function oneBlog(?string $uuid)
    {
        if (!$uuid) {
            return $this->failedResponse('Blog not found', [], [], 404);
        }
        $post = Post::whereUuid($uuid)->first();
        if (!$post) {
            return $this->failedResponse('Blog not found', [], [], 404);
        }

        return $this->successfulResponse(['blog' => $post]);
    }
}
