<?php

namespace App\Http\Controllers\Api\V1;

use Exception;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * @OA\Get(
     *    path="/api/v1/categories",
     *    summary="List all categories",
     *    operationId="categories-listings",
     *    tags={"Categories"},
     *    @OA\Parameter(
     *        name="page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(
     *        name="limit",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(
     *        name="sortBy",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Parameter(
     *        name="desc",
     *        required=false,
     *        in="query",
     *        @OA\Schema(type="boolean")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::query();
        return $this->paginate($request, $categories, route('api.v1.category.index'));
    }

    /**
     * @OA\Post(
     *    path="/api/v1/category/create",
     *    summary="Create a new category",
     *    operationId="categories-create",
     *    tags={"Categories"},
     *    @OA\RequestBody(
     *        required=true,
     *        @OA\MediaType(
     *            mediaType="application/x-www-form-urlencoded",
     *            @OA\Schema(
     *                type="object",
     *                required={"title"},
     *                @OA\Property(
     *                    property="title",
     *                    type="string",
     *                    description="Brand title"
     *                )
     *            )
     *        )
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('title');
        $validator = validator($data, [
            'title' => 'required|string'
        ]);
        if ($validator->fails()) {
            return $this->failedResponse('Failed Validation', $validator->errors(), [], 422);
        }
        try {
            $data = $validator->validated();
            $category = Category::create($data);
            return $this->successfulResponse($category);
        } catch (Exception $e) {
            return $this->failedResponse('Failed to create category', [$e->getMessage()], [], 422);
        }
    }

    /**
     * @OA\Get(
     *    path="/api/v1/category/{uuid}",
     *    summary="Fetch a category",
     *    operationId="categories-read",
     *    tags={"Categories"},
     *    @OA\Parameter(
     *        name="uuid",
     *        required=true,
     *        in="path",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param string|null $uuid
     * @return \Illuminate\Http\Response
     */
    public function show(?string $uuid = null)
    {
        if ($uuid == null) {
            return $this->failedResponse('Failed to find category', [], [], 404);
        }
        $category = Category::whereUuid($uuid)->first();
        if ($category == null) {
            return $this->failedResponse('Failed to find category', [], [], 404);
        }
        return $this->successfulResponse($category);
    }

    /**
     * @OA\Put(
     *    path="/api/v1/category/{uuid}",
     *    summary="Update an existing category",
     *    operationId="categories-update",
     *    tags={"Categories"},
     *    @OA\Parameter(
     *        name="uuid",
     *        required=true,
     *        in="path",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\RequestBody(
     *        required=true,
     *        @OA\MediaType(
     *            mediaType="application/x-www-form-urlencoded",
     *            @OA\Schema(
     *                type="object",
     *                required={"title"},
     *                @OA\Property(
     *                    property="title",
     *                    type="string",
     *                    description="Brand title"
     *                )
     *            )
     *        )
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param \Illuminate\Http\Request $request
     * @param string|null $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ?string $uuid = null)
    {
        if ($uuid == null) {
            return $this->failedResponse('Failed to find category', [], [], 404);
        }
        $category = Category::whereUuid($uuid)->first();
        if ($category == null) {
            return $this->failedResponse('Failed to find category', [], [], 404);
        }
        $data = $request->only('title');
        $validator = validator($data, [
            'title' => 'required|string'
        ]);
        if ($validator->fails()) {
            return $this->failedResponse('Failed Validation', $validator->errors(), [], 422);
        }
        try {
            $data = $validator->validated();
            $category->update($data);
            return $this->successfulResponse($category);
        } catch (Exception $e) {
            return $this->failedResponse('Failed to update category', [$e->getMessage()], [], 422);
        }
    }

    /**
     * @OA\Delete(
     *    path="/api/v1/category/{uuid}",
     *    summary="Delete an existing category",
     *    operationId="categories-delete",
     *    tags={"Categories"},
     *    @OA\Parameter(
     *        name="uuid",
     *        required=true,
     *        in="path",
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Response(response="200",description="OK"),
     *    @OA\Response(response="401",description="Unauthorized"),
     *    @OA\Response(response="404",description="Page not found"),
     *    @OA\Response(response="422",description="Unproccessable Entity"),
     *    @OA\Response(response="500",description="Internal server error")
     * )
     *
     * @param string|null $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy(?string $uuid)
    {
        if ($uuid == null) {
            return $this->failedResponse('Failed to find category', [], [], 404);
        }
        $category = Category::whereUuid($uuid)->first();
        if ($category == null) {
            return $this->failedResponse('Failed to find category', [], [], 404);
        }
        $category->products()->delete();
        $category->delete();
        return $this->successfulResponse([]);
    }
}
