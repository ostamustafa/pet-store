<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\JWTToken;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Lcobucci\JWT\Token\Plain;
use Lcobucci\JWT\Configuration;
use Illuminate\Support\MessageBag;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * @OA\Info(
 *     title = "Pet Shop API - Swagger Documentation",
 *     version = "1.0.0",
 * ),
 * @OA\Tag(
 *     name="Admin",
 *     description="Admin API Endpoint"
 * ),
 * @OA\Tag(
 *     name="User",
 *     description="User API Endpoint"
 * ),
 * @OA\Tag(
 *     name="Files",
 *     description="Files API Endpoint"
 * ),
 * @OA\Tag(
 *     name="Brands",
 *     description="Brands API Endpoint"
 * ),
 * @OA\Tag(
 *     name="Categories",
 *     description="Categories API Endpoint"
 * ),
 * @OA\Tag(
 *     name="Main",
 *     description="Main API Endpoint"
 * ),
 * @OA\Tag(
 *     name="Order Statuses",
 *     description="Order Statuses API Endpoint"
 * ),
 *
 * @param \Illuminate\Http\Request $request
 * @return \Illuminate\Http\JsonResponse
 */
class Controller extends BaseController
{
    use DispatchesJobs;
    use ValidatesRequests;
    use AuthorizesRequests;

    protected function successfulResponse(mixed $data, ?int $statusCode = 200, ?array $extra = [])
    {
        return $this->response(true, $data, null, [], $extra, null, $statusCode);
    }

    protected function paginate(Request $request, Builder $query, $path)
    {
        $limit = $request->input('limit', 10);
        $sortBy = $request->input('sortBy');
        $desc = $request->input('desc');
        if ($sortBy) {
            if (!$desc) {
                $query->orderBy($sortBy);
            } else {
                $query->orderByDesc($sortBy);
            }
        }
        $pagination = $query->paginate($limit);
        $currentPage = $pagination->currentPage();
        $from = 1 + ($limit * ($currentPage - 1));
        $total = $pagination->total();
        $to = $total < $limit ? $total : $limit * $currentPage;
        return $this->paginationResponse(
            $currentPage,
            $pagination,
            $from,
            $to,
            $limit,
            $path
        );
    }

    protected function getToken(User $user): Plain
    {
        $jwtToken = JWTToken::whereUserId($user->id)
            ->where('expires_at', '>', now())
            ->first();

        $tokenConfig = app(Configuration::class);
        if ($jwtToken) {
            $expiresAt = $jwtToken->expires_at->toDateTimeImmutable();
            $uuid = $jwtToken->unique_id;
            $jwtToken->refreshed_at = now();
            $jwtToken->save();
        } else {
            $expiresAt = now()->addHour()->toDateTimeImmutable();
            $uuid = Str::uuid();
            JWTToken::create([
                'user_id' => $user->id,
                'unique_id' => $uuid,
                'expires_at' => $expiresAt,
                'permissions' => '*',
                'token_title' => 'admin login'
            ]);
        }
        $user->last_login_at = now();
        $user->save();
        return $tokenConfig->builder()
            ->issuedBy(config('app.url'))
            ->permittedFor(config('app.url'))
            ->identifiedBy($uuid)
            ->withClaim('user_uuid', $user->uuid)
            ->expiresAt($expiresAt)
            ->getToken($tokenConfig->signer(), $tokenConfig->signingKey());
    }

    protected function response(
        int $success,
        mixed $data,
        ?string $error,
        array|MessageBag|null $errors,
        ?array $extra,
        ?array $trace,
        int $statusCode
    ) {
        $response = [
            'success' => $success,
            'data' => $data,
            'error' => $error,
            'errors' => $errors,
        ];
        if ($success) {
            $response['extra'] = $extra;
        } else {
            $response['trace'] = $trace;
        }
        return response()->json($response, $statusCode);
    }

    protected function failedResponse(
        string $error,
        array|MessageBag $errors,
        ?array $trace = [],
        ?int $statusCode = 400
    ) {
        return $this->response(false, [], $error, $errors, null, $trace, $statusCode);
    }

    protected function paginationResponse(
        int $currentPage,
        LengthAwarePaginator $paginator,
        int $from,
        int $to,
        int $perPage,
        string $path
    ) {
        return response()->json([
            'current_page' => $currentPage,
            'data' => $paginator->items(),
            'first_page_url' => $paginator->url(1),
            'next_page_url' => $paginator->nextPageUrl(),
            'prev_page_url' => $paginator->previousPageUrl(),
            'last_page_url' => $paginator->url($paginator->lastPage()),
            'links' => $paginator->linkCollection(),
            'from' => $from,
            'to' => $to,
            'total' => $paginator->total(),
            'per_page' => $perPage,
            'path' => $path,
        ]);
    }
}
