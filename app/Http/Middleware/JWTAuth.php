<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Carbon\Carbon;
use App\Models\User;
use App\Models\JWTToken;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Lcobucci\JWT\Configuration;
use Illuminate\Http\JsonResponse;
use Lcobucci\JWT\Validation\Constraint\IssuedBy;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Validation\Constraint\PermittedFor;

class JWTAuth
{
    /**
     * checks if the request contains a bearer token, if not then a 401 response is returned
     * <p>
     * validates the incoming jwt token with the following validators:
     * <ul>
     * <li>{@link IssuedBy} constraint</li>
     * <li>{@link PermittedFor} constraint</li>
     * <li>{@link SignedWith} constraint</li>
     * <ul>
     * if the validator fails, a 401 response is returned
     * <p>
     * checks if the jwt token has expired or not. If it did expire, a 401 response is returned
     * <p>
     * gets the user and jwt token records from the database, if either of them is null, a 401 response is returned
     * <p>
     * if all validations and constraints pass, the auth user is set
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(Request $request, Closure $next): JsonResponse|Response
    {
        if (!$request->bearerToken()) {
            return response()->json([
                'success' => 0,
                'data' => [],
                'error' => 'Failed to authenticate user',
                'errors' => [],
                'trace' => []
            ], 401);
        }
        $token = $request->bearerToken();
        $tokenConfig = app(Configuration::class);
        try {
            $jwtToken = $tokenConfig->parser()->parse($token);
            $tokenConfig->setValidationConstraints(new IssuedBy(config('app.url')));
            $tokenConfig->setValidationConstraints(new PermittedFor(config('app.url')));
            $tokenConfig->setValidationConstraints(new SignedWith($tokenConfig->signer(), $tokenConfig->signingKey()));
            $constraints = $tokenConfig->validationConstraints();
            if (!$tokenConfig->validator()->validate($jwtToken, ...$constraints)) {
                return response()->json([
                    'success' => 0,
                    'data' => [],
                    'error' => 'Failed to authenticate user',
                    'errors' => [],
                    'trace' => []
                ], 401);
            }
            $exp = $jwtToken->claims()->get('exp');
            $hasExpired = Carbon::parse($exp) < now();
            if ($hasExpired) {
                return response()->json([
                    'success' => 0,
                    'data' => [],
                    'error' => 'Failed to authenticate user',
                    'errors' => [],
                    'trace' => []
                ], 401);
            }

            $jti = $jwtToken->claims()->get('jti');
            $userUuid = $jwtToken->claims()->get('user_uuid');

            $user = User::where('uuid', $userUuid)->first();
            if (!$user) {
                return response()->json([
                    'success' => 0,
                    'data' => [],
                    'error' => 'Failed to authenticate user',
                    'errors' => [],
                    'trace' => []
                ], 401);
            }
            $jwtToken = JWTToken::where('unique_id', $jti)->where('user_id', $user->id)->first();
            if (!$jwtToken) {
                return response()->json([
                    'success' => 0,
                    'data' => [],
                    'error' => 'Failed to authenticate user',
                    'errors' => [],
                    'trace' => []
                ], 401);
            }
            $jwtToken->last_used_at = now();
            $jwtToken->save();
            auth()->setUser($user);
            return $next($request);
        } catch (Exception) {
            return response()->json([
                'success' => 0,
                'data' => [],
                'error' => 'Failed to authenticate user',
                'errors' => [],
                'trace' => []
            ], 401);
        }
    }
}
