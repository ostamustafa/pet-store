<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdminSeeder::class,
            BrandSeeder::class,
            CategorySeeder::class,
            FileSeeder::class,
            UserSeeder::class,
            OrderStatusSeeder::class,
            PaymentSeeder::class,
            PostSeeder::class,
            PromotionSeeder::class,
            ProductSeeder::class,
            OrderSeeder::class,
        ]);
    }
}
