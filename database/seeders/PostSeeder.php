<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use App\Models\File;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::factory()
            ->count(10)
            ->state(function () {
                $user = User::inRandomOrder()->first();
                return [
                    'metadata' => [
                        'author' => "$user->first_name $user->last_name",
                        'image' => File::inRandomOrder()->first()->uuid
                    ]
                ];
            })->create();
    }
}
