<?php

namespace Database\Seeders;

use App\Models\File;
use App\Models\Promotion;
use Illuminate\Database\Seeder;

class PromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Promotion::factory()
            ->count(10)
            ->state(function () {
                return [
                    'metadata' => [
                        'valid_from' => now()->toDateString(),
                        'valid_to' => now()->addMonth()->toDateString(),
                        'image' => File::inRandomOrder()->first()->uuid
                    ]
                ];
            })->create();
    }
}
