<?php

namespace Database\Seeders;

use App\Models\OrderStatus;
use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titles = [
            'open',
            'pending payment',
            'paid',
            'shipped',
            'cancelled'
        ];
        foreach ($titles as $title) {
            OrderStatus::factory(['title' => $title])->create();
        }
    }
}
