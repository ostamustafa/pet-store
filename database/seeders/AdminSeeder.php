<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory([
            'is_admin' => true,
            'email' => 'admin@buckhill.co.uk',
            'is_marketing' => false,
            'password' => Hash::make('admin')
        ])->create();
    }
}
