<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Product;
use App\Models\OrderStatus;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::factory()
            ->count(50)
            ->state(function () {
                $products = Product::inRandomOrder()->limit(5)->get();
                $productsPayload = [];
                foreach ($products as $product) {
                    $productsPayload[] = [
                        'product' => $product->uuid,
                        'quantity' => 4
                    ];
                }
                return [
                    'user_id' => User::inRandomOrder()->first()->id ?? 1,
                    'order_status_id' => OrderStatus::inRandomOrder()->first()->id,
                    'payment_id' => Payment::inRandomOrder()->first()->id,
                    'products' => $productsPayload
                ];
            })->create();
    }
}
