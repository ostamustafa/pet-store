<?php

namespace Database\Seeders;

use App\Models\File;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::factory()
            ->count(50)
            ->state(function () {
                return [
                    'category_uuid' => Category::inRandomOrder()->first()->uuid,
                    'metadata' => [
                        'brand' => Brand::inRandomOrder()->first()->uuid,
                        'image' => File::inRandomOrder()->first()->uuid
                    ]
                ];
            })->create();
    }
}
