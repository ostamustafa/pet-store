<?php

namespace Database\Factories;

use App\Models\File;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\Factory;

class FileFactory extends Factory
{

    protected $model = File::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    #[ArrayShape([
        'uuid' => "string",
        'name' => "string",
        'path' => "string",
        'type' => "string",
        'size' => "string"
    ])] public function definition(): array
    {
        $path = $this->faker->image(Storage::path('public/pet-store'));
        $basename = basename($path);
        $path = "public/pet-store/$basename";
        $size = $this->getSize(Storage::size($path));
        $type = Storage::mimeType($path);
        $name = Str::random();
        $uuid = $this->faker->uuid;
        return [
            'uuid' => $uuid,
            'name' => $name,
            'path' => $path,
            'type' => $type,
            'size' => $size,
        ];
    }

    private function getSize(float $size): string
    {
        if ($size < 1024) {
            $size = number_format($size, 2);
            return "$size B";
        }
        $size = $size / 1024;
        if ($size < 1024) {
            $size = number_format($size, 2);
            return "$size KB";
        }

        $size = $size / 1024;
        if ($size < 1024) {
            $size = number_format($size, 2);
            return "$size MB";
        }
        $size = $size / 1024;
        $size = number_format($size, 2);
        return "$size GB";
    }
}
