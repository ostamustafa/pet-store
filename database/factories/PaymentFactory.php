<?php

namespace Database\Factories;

use App\Models\Payment;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentFactory extends Factory
{

    protected $model = Payment::class;

    private array $defaultTypes = [
        'credit_card',
        'cash_on_delivery',
        'bank_transfer'
    ];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    #[ArrayShape([
        'uuid' => "string",
        'details' => "array",
        'type' => "mixed"
    ])] public function definition(): array
    {
        $type = $this->faker->randomElement($this->defaultTypes);
        //credit card
        if ($type == $this->defaultTypes[0]) {
            $details = [
                'holder_name' => $this->faker->name,
                'number' => $this->faker->creditCardNumber,
                'ccv' => $this->faker->randomNumber(3, true),
                'expire_date' => $this->faker->creditCardExpirationDate
            ];
        } //cash_on_delivery
        elseif ($type == $this->defaultTypes[1]) {
            $details = [
                'first_name' => $this->faker->firstName,
                'last_name' => $this->faker->lastName,
                'address' => $this->faker->address
            ];
        } //bank_transfer
        else {
            $details = [
                'swift' => $this->faker->swiftBicNumber,
                'iban' => $this->faker->iban(),
                'name' => $this->faker->name
            ];
        }
        return [
            'uuid' => $this->faker->uuid,
            'details' => $details,
            'type' => $type
        ];
    }
}
