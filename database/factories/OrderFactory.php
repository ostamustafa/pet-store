<?php

namespace Database\Factories;

use App\Models\Order;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{

    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    #[ArrayShape([
        'uuid' => "string",
        'address' => "array",
        'amount' => "float",
        'delivery_fee' => "int",
        'shipped_at' => "\DateTime"
    ])] public function definition(): array
    {
        $address = $this->faker->address;
        $addressJson = [
            'billing' => $address,
            'shipping' => $address
        ];
        $amount = $this->faker->randomFloat(2, 1, 1000);
        return [
            'uuid' => $this->faker->uuid,
            'address' => $addressJson,
            'amount' => $amount,
            'delivery_fee' => $amount > 500 ? 15 : 0,
            'shipped_at' => $this->faker->dateTime
        ];
    }
}
