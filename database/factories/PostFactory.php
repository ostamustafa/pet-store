<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{

    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    #[ArrayShape([
        'uuid' => "string",
        'title' => "array|string",
        'slug' => "string",
        'content' => "string"
    ])] public function definition(): array
    {
        $title = implode(" ", $this->faker->words(5));
        return [
            'uuid' => $this->faker->uuid,
            'title' => $title,
            'slug' => Str::slug($title),
            'content' => $this->faker->text
        ];
    }
}
