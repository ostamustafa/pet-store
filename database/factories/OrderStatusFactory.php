<?php

namespace Database\Factories;

use App\Models\OrderStatus;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderStatusFactory extends Factory
{

    protected $model = OrderStatus::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    #[ArrayShape([
        'uuid' => "string"
    ])] public function definition(): array
    {
        return [
            'uuid' => $this->faker->uuid
        ];
    }
}
