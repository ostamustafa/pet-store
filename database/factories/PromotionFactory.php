<?php

namespace Database\Factories;

use App\Models\Promotion;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Database\Eloquent\Factories\Factory;

class PromotionFactory extends Factory
{

    protected $model = Promotion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    #[ArrayShape([
        'uuid' => "string",
        'title' => "array|string",
        'content' => "string"
    ])] public function definition(): array
    {
        return [
            'uuid' => $this->faker->uuid,
            'title' => implode(" ", $this->faker->words(5)),
            'content' => $this->faker->text,
        ];
    }
}
