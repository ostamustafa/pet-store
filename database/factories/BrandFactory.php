<?php

namespace Database\Factories;

use App\Models\Brand;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Database\Eloquent\Factories\Factory;

class BrandFactory extends Factory
{

    protected $model = Brand::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    #[ArrayShape([
        'title' => "array|string",
        'slug' => "string",
        'uuid' => "string"
    ])] public function definition(): array
    {
        $title = $this->faker->words(4);
        $title = implode(" ", $title);
        return [
            'title' => $title,
            'slug' => Str::slug($title),
            'uuid' => $this->faker->uuid
        ];
    }
}
