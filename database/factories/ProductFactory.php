<?php

namespace Database\Factories;

use App\Models\Product;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{

    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    #[ArrayShape([
        'uuid' => "string",
        'title' => "array|string",
        'description' => "string",
        'price' => "float"
    ])] public function definition(): array
    {
        return [
            'uuid' => $this->faker->uuid,
            'title' => implode(" ", $this->faker->words(5)),
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 1, 50)
        ];
    }
}
