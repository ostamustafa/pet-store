<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{

    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    #[ArrayShape([
        'uuid' => "string",
        'title' => "array|string",
        'slug' => "string"
    ])] public function definition(): array
    {
        $title = $this->faker->words(5);
        $title = implode(' ', $title);
        return [
            'uuid' => $this->faker->uuid,
            'title' => $title,
            'slug' => Str::slug($title)
        ];
    }
}
