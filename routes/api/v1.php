<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\UserController;
use App\Http\Controllers\Api\V1\MainController;
use App\Http\Controllers\Api\V1\FileController;
use App\Http\Controllers\Api\V1\AdminController;
use App\Http\Controllers\Api\V1\BrandController;
use App\Http\Controllers\Api\V1\CategoryController;
use App\Http\Controllers\Api\V1\OrderStatusController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('guest')->group(function () {
    Route::post('admin/login', [AdminController::class, 'login'])
        ->name('admin.login');
    Route::post('user/login', [UserController::class, 'login'])
        ->name('user.login');
});
Route::middleware('auth.jwt')->group(function () {
    Route::name('admin.')
        ->middleware('check.admin')
        ->controller(AdminController::class)
        ->prefix('admin')
        ->group(function () {
            Route::get('logout', 'logout')->name('logout');
            Route::post('create', 'store')->name('create');
            Route::get('user-listing', 'userListing')->name('user_listing');
            Route::delete('user-delete/{uuid?}', 'userDelete')->name('user_delete');
            Route::put('user-edit/{uuid?}', 'userEdit')->name('user_edit');
        });
    Route::apiResource('category', CategoryController::class)
        ->middleware('check.admin')
        ->parameters([
            'category' => 'uuid?'
        ])
        ->only(['update', 'store', 'destroy']);
    Route::apiResource('brand', BrandController::class)
        ->middleware('check.admin')
        ->parameters([
            'brand' => 'uuid?'
        ])
        ->only(['update', 'store', 'destroy']);
    Route::apiResource('order-status', OrderStatusController::class)
        ->middleware('check.admin')
        ->parameters([
            'order-status' => 'uuid?'
        ])
        ->only(['update', 'store', 'destroy']);
    Route::name('user.')
        ->controller(UserController::class)
        ->prefix('user')
        ->group(function () {
            Route::get('', 'show')->name('show');
            Route::delete('', 'destroy')->name('destroy');
            Route::get('orders', 'orders')->name('orders');
            Route::put('edit', 'update')->name('edit');
            Route::get('logout', 'logout')->name('logout');
        });
    Route::post('file/upload', [FileController::class, 'upload'])
        ->name('file.upload');
});
Route::name('main.')->prefix('main')->controller(MainController::class)->group(function () {
    Route::get('promotions', 'promotions')->name('promotions');
    Route::get('blog', 'blog')->name('blog');
    Route::get('blog/{uuid?}', 'oneBlog')->name('blog_one');
});
Route::get('categories', [CategoryController::class, 'index'])
    ->name('category.index');
Route::get('category/{uuid?}', [CategoryController::class, 'show'])
    ->name('category.show');
Route::get('brands', [BrandController::class, 'index'])
    ->name('brand.index');
Route::get('brand/{uuid?}', [BrandController::class, 'show'])
    ->name('brand.show');
Route::get('order-statuses', [OrderStatusController::class, 'index'])
    ->name('order-status.index');
Route::get('order-status/{uuid?}', [OrderStatusController::class, 'show'])
    ->name('order-status.show');
Route::get('file/{uuid?}', [FileController::class, 'show'])
    ->name('file.show');